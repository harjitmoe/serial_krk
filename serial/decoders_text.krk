from serial import utilities, decoder_basis, wrappers, scalar_parsers
import codecs

let _hex_alpha = "0123456789ABCDEF"
def _unhexlify(str_data):
    let ret_builder = [None] * (len(str_data) // 2)
    for n in range(0, len(str_data), 2):
        ret_builder[n//2] = _hex_alpha.index(str_data[n]) << 4 | _hex_alpha.index(str_data[n+1])
    return bytes(ret_builder)

class JsLikeStringDeserialiser(decoder_basis.TextualDeserialiser):

    def __init__(pedantic_escapes=False, **kwargs):
        super().__init__(**kwargs)
        self.pedantic_escapes = pedantic_escapes
    
    def read_string(data, start):
        assert start < len(data) and data[start] == '"'
        let ptrfrom = start + 1
        let ptr = ptrfrom
        let builder = []
        while ptr < len(data):
            if data[ptr] == data[start]:
                builder.append(data[ptrfrom:ptr])
                ptr += 1
                return "".join(builder), ptr
            else if data[ptr] == "\\":
                builder.append(data[ptrfrom:ptr])
                if ptr == len(data):
                    break
                ptr += 1
                let escchar = data[ptr]
                # RFC 7159 escapes are b/f/n/r/t and \uXXXX (not \v, \xXX, \u{X…}, or \UXXXXXXXX).
                #   Only literal characters that can be escaped are \, " and / (not ', and though
                #   \/ is pointless and also not accepted by all readers, e.g. older YAML readers).
                #   Accepting further escapes might be reasonable, but they oughtn't be emitted.
                if escchar == "b":
                    builder.append("\b")
                    ptr += 1
                else if escchar == "f":
                    builder.append("\f")
                    ptr += 1
                else if escchar == "n":
                    builder.append("\n")
                    ptr += 1
                else if escchar == "r":
                    builder.append("\r")
                    ptr += 1
                else if escchar == "t":
                    builder.append("\t")
                    ptr += 1
                else if escchar in ("\\", '"', "/"):
                    builder.append(escchar)
                    ptr += 1
                else if escchar == "u":
                    if (not self.pedantic_escapes) and (ptr + 1) < len(data) and data[ptr + 1] == "{":
                        ptr += 2
                        let startptr = ptr
                        while ptr < len(data) and data[ptr] != "}":
                            ptr += 1
                        let hexdata = data[startptr:ptr]
                        if hexdata.lower().strip("0123456789abcdef"):
                            raise utilities.InvalidSerialisationError(
                                f"invalid variable-width Unicode escape at index {startptr-3}")
                        else if ptr == len(data):
                            break
                        else:
                            ptr += 1
                        builder.append(chr(int(hexdata, 16)))
                    else:
                        let hexdata = data[ptr+1:ptr+5]
                        if hexdata.lower().strip("0123456789abcdef"):
                            raise utilities.InvalidSerialisationError(
                                f"invalid Unicode escape at index {ptr-1}")
                        else if len(hexdata) < 4:
                            break
                        ptr += 5
                        let code_unit = int(hexdata, 16)
                        if 0xD800 <= code_unit < 0xDC00 and data[ptr:ptr+4].lower() in (
                                "\\udc", "\\udd", "\\ude", "\\udf"):
                            let second_hexdata = data[ptr+2:ptr+6]
                            if second_hexdata.lower().strip("0123456789abcdef"):
                                raise utilities.InvalidSerialisationError(
                                    f"invalid Unicode escape at index {ptr}")
                            let utf16be = bytes([
                                int(hexdata[:2], 16),
                                int(hexdata[2:], 16),
                                int(second_hexdata[:2], 16),
                                int(hexdata[2:], 16)])
                            ptr += 6
                            builder.append(codecs.decode(utf16be, "utf-16be"))
                        else:
                            builder.append(chr(code_unit))
                # Extended-only escapes
                else if (not self.pedantic_escapes) and escchar == "x":
                    let hexdata = data[ptr+1:ptr+3]
                    if hexdata.lower().strip("0123456789abcdef"):
                        raise utilities.InvalidSerialisationError(
                            f"invalid Latin-1 escape at index {ptr-1}")
                    else if len(hexdata) < 2:
                        break
                    builder.append(chr(int(hexdata, 16)))
                    ptr += 3
                else if (not self.pedantic_escapes) and escchar == "U":
                    let hexdata = data[ptr+1:ptr+9]
                    if hexdata.lower().strip("0123456789abcdef"):
                        raise utilities.InvalidSerialisationError(
                            f"invalid UCS-4 escape at index {ptr-1}")
                    else if len(hexdata) < 8:
                        break
                    builder.append(chr(int(hexdata, 16)))
                    ptr += 9
                else if (not self.pedantic_escapes) and escchar in "01234567":
                    let octwidth = 1
                    if (ptr + 1) < len(data) and data[ptr+1] in "01234567":
                        if (ptr + 2) < len(data) and data[ptr+2] in "01234567":
                            octwidth = 3
                        else:
                            octwidth = 2
                    else:
                        octwidth = 1
                    let octdata = data[ptr:ptr+octwidth]
                    builder.append(chr(int(octdata, 8)))
                    ptr += octwidth
                else if (not self.pedantic_escapes) and escchar == "v":
                    builder.append("\v")
                    ptr += 1
                else if (not self.pedantic_escapes) and escchar == "'":
                    builder.append("'")
                    ptr += 1
                else if (not self.pedantic_escapes) and escchar in ("e", "["):
                    builder.append("\x1B")
                    ptr += 1
                else:
                    raise utilities.InvalidSerialisationError(f"unrecognised escape at index {ptr-1}")
                ptrfrom = ptr
            else:
                ptr += 1
        raise utilities.InvalidSerialisationError(f"unterminated string starting at index {start}")

class JsonDeserialiser(JsLikeStringDeserialiser):

    def __init__(pedantic_ternaries=False, **kwargs):
        super().__init__(**kwargs)
        self.pedantic_ternaries = pedantic_ternaries
    
    def _is_scalar(data, start):
        return (not data) or data[start:start+1] not in ("{", "[")
    
    @staticmethod
    def _simple_get_end(data, start):
        let end = start
        while end < len(data) and data[end].strip() and data[end] not in ":,}]":
            end += 1
        return end
    
    def _dispatch_scalar(data, start):
        if data[start:start+1] == '"':
            return self.read_string
        let end = self._simple_get_end(data, start)
        if end > start and data[start] in "-+0123456789":
            if data[start] in "-+" and data[start+1:end] in ("Infinity", "NaN"):
                return self.read_float
            else if "." in data[start:end]:
                return self.read_float
            else if data[start:start+2].lower() != "0x" and ("e" in data[start:end] or "E" in data[start:end]):
                return self.read_float
            return self.read_int
        else if data[start:end] in ("Infinity", "NaN"):
            return self.read_float
        else:
            return self.read_ternary
    
    def read_int(data, start):
        let end = self._simple_get_end(data, start)
        return scalar_parsers.interpret_int(data[start:end], self.leading_zero_is_octal), end
    
    def read_ternary(data, start):
        let end = self._simple_get_end(data, start)
        let value = scalar_parsers.interpret_ternary(data[start:end])
        if self.pedantic_ternaries:
            let standard = {True: "true", False: "false", None: "null"}[value]
            if data[start:end] != standard:
                raise utilities.InvalidSerialisationError(
                    f"nonstandard JSON at index {start}: {data[start:end]!r} for {standard!r}")
        return value, end
    
    def read_float(data, start):
        let end = self._simple_get_end(data, start)
        return scalar_parsers.interpret_float(data[start:end], self.use_wrappers), end
    
    def _dispatch_compound(data, start):
        if data[start:start+1] == "{":
            return self.read_dict
        else if data[start:start+1] == "[":
            return self.read_list
        else:
            raise AssertionError(data[:80])
    
    def read_list(data, start):
        assert start < len(data) and data[start] == "["
        let ptr = start + 1
        let builder = []
        while ptr < len(data):
            if not data[ptr].strip():
                ptr += 1
                continue
            else if data[ptr] == "]":
                ptr += 1
                return builder, ptr
            else:
                let subobj
                subobj, ptr = self._deserialise_unit(data, ptr)
                builder.append(subobj)
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == ",":
                    ptr += 1
                else if data[ptr] == "]":
                    continue
                else:
                    raise utilities.InvalidSerialisationError(
                            f"expecting comma or end of list at index {ptr}")
        raise utilities.InvalidSerialisationError(f"unterminated list starting at index {start}")
    
    def read_dict(data, start):
        assert start < len(data) and data[start] == "{"
        let ptr = start + 1
        let builder = {}
        while ptr < len(data):
            if not data[ptr].strip():
                ptr += 1
                continue
            else if data[ptr] == "}":
                ptr += 1
                return builder, ptr
            else:
                let key, value
                key, ptr = self._deserialise_unit(data, ptr)
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] != ":":
                    raise utilities.InvalidSerialisationError(f"expecting colon at index {ptr}")
                ptr += 1
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                value, ptr = self._deserialise_unit(data, ptr)
                builder[key] = value
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == ",":
                    ptr += 1
                else if data[ptr] == "}":
                    continue
                else:
                    raise utilities.InvalidSerialisationError(
                            f"expecting comma or end of dict at index {ptr}")
        raise utilities.InvalidSerialisationError(f"unterminated dict starting at index {start}")

class PlistTextDeserialiser(JsLikeStringDeserialiser):

    def __init__(**kwargs):
        super().__init__(pedantic_escapes=False, **kwargs)
    
    def _is_scalar(data, start):
        return (not data) or data[start:start+1] not in ("{", "(")
    
    @staticmethod
    def _simple_get_end(data, start):
        let end = start
        while end < len(data) and data[end].strip() and data[end] not in " ;,=)]}>":
            end += 1
        return end
    
    def _dispatch_scalar(data, start):
        if data[start:start+1] == '"':
            return self.read_string
        else if data[start:start+3] == "<*I":
            return self.read_int
        else if data[start:start+3] == "<*R":
            return self.read_float
        else if data[start:start+3] in ("<*B", "<*N"):
            return self.read_ternary
        else if data[start:start+3] == "<*D":
            return self.read_timestamp
        else if data[start:start+2] == "<[":
            return self.read_base64
        else if data[start:start+1] == "<":
            return self.read_hexadecimal
        else:
            return self.read_string

    def read_base64(data, start):
        assert data[start:start+2] == "<["
        let end = self._simple_get_end(data, start + 2)
        if data[end:end+2] != "]>":
            raise utilities.InvalidSerialisationError(
                f"truncated text-plist base64 extension at index {start}")
        return codecs.encode(data[start+2:end], "inverse-base64"), end+2

    def read_hexadecimal(data, start):
        assert data[start:start+1] == "<"
        let end = self._simple_get_end(data, start + 1)
        if data[end:end+1] != ">":
            raise utilities.InvalidSerialisationError(
                f"truncated text-plist base64 extension at index {start}")
        return _unhexlify(data[start+1:end]), end+1
    
    def read_int(data, start):
        assert data[start:start+3] == "<*I"
        let end = self._simple_get_end(data, start + 3)
        if data[end:end+1] != ">":
            raise utilities.InvalidSerialisationError(
                f"truncated text-plist integer extension at index {start}")
        return scalar_parsers.interpret_int(data[start+3:end], self.leading_zero_is_octal), end+1
    
    def read_ternary(data, start):
        if data[start:start+5] == "<*BY>":
            return True, start+5
        else if data[start:start+5] == "<*BN>":
            return False, start+5
        else if data[start:start+4] == "<*N>":
            return None, start+4
        else:
            raise utilities.InvalidSerialisationError(
                f"expected text-plist null or boolean extension at index {start}")
    
    def read_float(data, start):
        assert data[start:start+3] == "<*R"
        let end = self._simple_get_end(data, start + 3)
        if data[end:end+1] != ">":
            raise utilities.InvalidSerialisationError(
                f"truncated text-plist real-number extension at index {start}")
        return scalar_parsers.interpret_float(data[start+3:end], self.use_wrappers), end+1
    
    def read_timestamp(data, start):
        assert data[start:start+3] == "<*D"
        let end = start + 3
        while end < len(data) and data[end] != ">":
            end += 1
        if data[end:end+1] != ">":
            raise utilities.InvalidSerialisationError(
                f"truncated text-plist timestamp extension at index {start}")
        return scalar_parsers.interpret_timestamp(data[start+3:end], self.use_wrappers), end+1
    
    def read_string(data, start):
        if data[start:start+1] == '"':
            return super().read_string(data, start)
        let end = self._simple_get_end(data, start)
        return data[start:end], end
    
    def _dispatch_compound(data, start):
        if data[start:start+1] == "{":
            return self.read_dict
        else if data[start:start+1] == "(":
            return self.read_list
        else:
            raise AssertionError(data[:80])
    
    def read_list(data, start):
        assert start < len(data) and data[start] == "("
        let ptr = start + 1
        let builder = []
        while ptr < len(data):
            if not data[ptr].strip():
                ptr += 1
                continue
            else if data[ptr] == ")":
                ptr += 1
                return builder, ptr
            else if data[ptr] == ",": # i.e. at start of list or after another comma
                ptr += 1
                builder.append(None)
            else:
                let subobj
                subobj, ptr = self._deserialise_unit(data, ptr)
                builder.append(subobj)
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == ",":
                    ptr += 1
                else if data[ptr] == ")":
                    continue
                else:
                    raise utilities.InvalidSerialisationError(
                            f"expecting comma or end of list at index {ptr}")
        raise utilities.InvalidSerialisationError(f"unterminated list starting at index {start}")
    
    def read_dict(data, start):
        assert start < len(data) and data[start] == "{"
        let ptr = start + 1
        let builder = {}
        while ptr < len(data):
            if not data[ptr].strip():
                ptr += 1
                continue
            else if data[ptr] == "}":
                ptr += 1
                return builder, ptr
            else:
                let key, value
                key, ptr = self._deserialise_unit(data, ptr)
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == "=":
                    ptr += 1
                    while ptr < len(data) and not data[ptr].strip():
                        ptr += 1
                    if ptr == len(data):
                        break
                    value, ptr = self._deserialise_unit(data, ptr)
                else if data[ptr] == ";":
                    value = None
                else:
                    raise utilities.InvalidSerialisationError(
                            f"expecting equals or semicolon at index {ptr}")
                builder[key] = value
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == ";":
                    ptr += 1
                else if data[ptr] == "}":
                    continue
                else:
                    raise utilities.InvalidSerialisationError(
                            f"expecting comma or end of dict at index {ptr}")
        raise utilities.InvalidSerialisationError(f"unterminated dict starting at index {start}")

class SnbtDeserialiser(decoder_basis.TextualDeserialiser):

    def __init__(**kwargs):
        super().__init__(**kwargs)
    
    def _is_scalar(data, start):
        return (not data) or data[start:start+1] not in ("{", "[")
    
    @staticmethod
    def _simple_get_end(data, start):
        let end = start
        while end < len(data) and data[end].strip() and data[end] not in " ;,:]}":
            end += 1
        return end
    
    def _dispatch_scalar(data, start):
        if data[start:start+1] in ('"', "'"):
            return self.read_string
        let end = self._simple_get_end(data, start)
        if data[start:start+1] in "0123456789" or (
                data[start:start+1] in "+-" and data[start+1:start+2] in "0123456789"):
            let section = data[start:end]
            if "." in section:
                return self.read_float
            else if ("e" in section or "E" in section) and section[:2].lower() != "0x":
                return self.read_float
            else:
                return self.read_int
        else if data[start:end] == "true" or data[start:end] == "false" or data[start:start+1] == "@":
            return self.read_ternary
        else:
            return self.read_string
    
    def read_int(data, start):
        let end = self._simple_get_end(data, start)
        let segment = data[start:end]
        let wrapper = int
        if segment[-1] in "bB":
            if wrappers.SInt8Wrapper in self.use_wrappers:
                wrapper = wrappers.SInt8Wrapper
            segment = segment[:-1]
        else if segment[-1] in "sS":
            if wrappers.SInt16Wrapper in self.use_wrappers:
                wrapper = wrappers.SInt16Wrapper
            segment = segment[:-1]
        else if segment[-1] in "lL":
            if wrappers.SInt64Wrapper in self.use_wrappers:
                wrapper = wrappers.SInt64Wrapper
            segment = segment[:-1]
        else:
            if wrappers.SInt32Wrapper in self.use_wrappers:
                wrapper = wrappers.SInt32Wrapper
        return wrapper(scalar_parsers.interpret_int(segment, self.leading_zero_is_octal)), end
    
    def read_ternary(data, start):
        let end = self._simple_get_end(data, start)
        return scalar_parsers.interpret_ternary(data[start:end].lstrip("@")), end
    
    def read_float(data, start):
        let end = self._simple_get_end(data, start)
        return scalar_parsers.interpret_float(data[start:end], self.use_wrappers), end
    
    def read_string(data, start):
        if data[start] not in ('"', "'"):
            let end = self._simple_get_end(data, start)
            return data[start:end], end
        let end = start + 1
        let builder = []
        while end < len(data) and data[end] != data[start]:
            if data[end] == "\\" and data[end+1:end+2] in ("\\", data[start]):
                end += 1
            builder.append(data[end])
            end += 1
        if end >= len(data):
            raise utilities.InvalidSerialisationError(f"truncated string at index {start}")
        end += 1
        return "".join(builder), end
    
    def _dispatch_compound(data, start):
        if data[start:start+1] == "{":
            return self.read_dict
        else if data[start:start+1] == "[":
            return self.read_list
        else:
            raise AssertionError(data[:80])
    
    @staticmethod
    def _byte_to_uint(i):
        let ret = i
        if isinstance(i, wrappers.UnwrappableWrapper):
            ret = i.get_value()
        if ret < 0:
            ret += 256
        return ret
    
    @staticmethod
    def _maybe_wrapped_to_bytes(inpt):
        return bytes([SnbtDeserialiser._byte_to_uint(i) for i in inpt])
    
    def read_list(data, start):
        assert start < len(data) and data[start] == "["
        let ptr = start + 1
        let kind = list
        if data[ptr:ptr+2] == "B;":
            kind = self._maybe_wrapped_to_bytes
            ptr += 2
        else if data[ptr:ptr+2] == "I;":
            if wrappers.SInt32ArrayWrapper in self.use_wrappers:
                kind = wrappers.SInt32ArrayWrapper
            ptr += 2
        else if data[ptr:ptr+2] == "L;":
            if wrappers.SInt64ArrayWrapper in self.use_wrappers:
                kind = wrappers.SInt64ArrayWrapper
            ptr += 2
        let builder = []
        while ptr < len(data):
            if not data[ptr].strip():
                ptr += 1
                continue
            else if data[ptr] == "]":
                ptr += 1
                return kind(builder), ptr
            else if data[ptr] == ",": # i.e. at start of list or after another comma
                ptr += 1
                builder.append(None)
            else:
                let subobj
                subobj, ptr = self._deserialise_unit(data, ptr)
                builder.append(subobj)
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == ",":
                    ptr += 1
                else if data[ptr] == "]":
                    continue
                else:
                    raise utilities.InvalidSerialisationError(
                            f"expecting comma or end of list at index {ptr}")
        raise utilities.InvalidSerialisationError(f"unterminated list starting at index {start}")
    
    def read_dict(data, start):
        assert start < len(data) and data[start] == "{"
        let ptr = start + 1
        let builder = {}
        while ptr < len(data):
            if not data[ptr].strip():
                ptr += 1
                continue
            else if data[ptr] == "}":
                ptr += 1
                return builder, ptr
            else:
                let key, value
                key, ptr = self._deserialise_unit(data, ptr)
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == ":":
                    ptr += 1
                    while ptr < len(data) and not data[ptr].strip():
                        ptr += 1
                    if ptr == len(data):
                        break
                    value, ptr = self._deserialise_unit(data, ptr)
                else:
                    raise utilities.InvalidSerialisationError(f"expecting colon at index {ptr}")
                builder[key] = value
                while ptr < len(data) and not data[ptr].strip():
                    ptr += 1
                if ptr == len(data):
                    break
                if data[ptr] == ",":
                    ptr += 1
                else if data[ptr] == "}":
                    continue
                else:
                    raise utilities.InvalidSerialisationError(
                            f"expecting comma or end of dict at index {ptr}")
        raise utilities.InvalidSerialisationError(f"unterminated dict starting at index {start}")


