#!/usr/bin/env sh

wget https://www.unicode.org/Public/UCD/latest/ucd/extracted/DerivedGeneralCategory.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/Scripts.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/ScriptExtensions.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/DerivedCoreProperties.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/extracted/DerivedName.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/PropList.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/PropertyAliases.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/PropertyValueAliases.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/NameAliases.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/NamedSequences.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/CaseFolding.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/extracted/DerivedEastAsianWidth.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/extracted/DerivedBidiClass.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/auxiliary/GraphemeBreakProperty.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/auxiliary/WordBreakProperty.txt
wget https://www.unicode.org/Public/UCD/latest/ucd/auxiliary/SentenceBreakProperty.txt

