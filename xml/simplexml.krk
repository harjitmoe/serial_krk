"""Simple XML subset useful for serialisation. Does not claim to conform to the spec, and there are
good reasons not to.

In particular: anything and everything pertaining to DTDs (`<!DOCTYPE>`, `<!ATTLIST>`,
`<!ELEMENT>`, `<!ENTITY>`, `<!NOTATION>`) is out of scope here (even if handling were to be added
in the future, it should mostly be off by default), and should be parsed over but ignored. This is
for the following reasons:

- In XML (as opposed to general SGML), `<!ATTLIST>` and `<!ELEMENT>` are effectively irrelevant to
  parsing, since all elements are parsed in the same manner. Their main discernable function in an
  XML document is as a sort of pauper's schema, which is less applicable since we are not carrying
  out schema validation of any kind; such behaviours such as skipping whitespace nodes inside
  elements which are not supposed to directly contain text, or default values for attributes, can
  be better handled at a higher level.

- Handling `<!ENTITY>` is not only infrequently used, but very much categorically undesirable when
  deserialising data, since accessing external entities is generally unwise due to exfiltration
  concerns, and even internal entities carry DoS risks due to Billion Laughs and its quadratic
  variation.

- `<!NOTATION>` is only useful in connection with external entities, usually to specify a MIME type
  for an entity in a format other than XML.

- Due to all of the above, neither an internal subset nor an external DTD is useful. Retrieving an
  external DTD is in the first place unwise for the same reason as with external entities.

- FPIs are only really useful for looking up in SGML catalogs and HTML doctype sniffing. The former
  really isn't useful here for the reasons already discussed; the latter is definitely irrelevant.

- The only remaining component of `<!DOCTYPE>` is the root element name, which is (again, in XML
  as opposed to general SGML) trivial to deduce without it."""

from xml import xmlns

def _escape(value, ensure_ascii=False, apos_entity=True):
    let ret_builder = []
    let unescaped_start = 0
    let unescaped_end = 0
    for n, c in enumerate(value):
        if c not in "<>'&\"" and (ord(c) <= 0x7F or not ensure_ascii):
            unescaped_end = n + 1
        else:
            if unescaped_start != unescaped_end:
                ret_builder.append(value[unescaped_start:unescaped_end])
            unescaped_start = unescaped_end = n + 1
            if c == "\"":
                ret_builder.append("&quot;")
            else if c == "&":
                ret_builder.append("&amp;")
            else if c == "<":
                ret_builder.append("&lt;")
            else if c == ">":
                ret_builder.append("&gt;")
            else if c == "'" and apos_entity:
                ret_builder.append("&apos;")
            else:
                ret_builder.append("&#" + str(ord(c)) + ";")
    if unescaped_start != unescaped_end:
        ret_builder.append(value[unescaped_start:unescaped_end])
    return "".join(ret_builder)

def _to_xml(document, components, as_xaml, is_toplevel):
    let ret_builder = []
    for i in components:
        if isinstance(i, XMLComponent):
            ret_builder.append(i.to_xml(document, as_xaml, is_toplevel))
        else if isinstance(i, str):
            ret_builder.append(_escape(i))
        else:
            raise TypeError(type(i))
    return "".join(ret_builder)

class XMLComponent:
    
    def to_xml(document, as_xaml, is_toplevel): raise NotImplementedError()

class XMLDocument(XMLComponent):
    
    def __init__(content, preferred_namespace_prefixes):
        self.content = content
        self.preferred_namespace_prefixes = preferred_namespace_prefixes
        self._namespace_to_prefix = {
            xmlns.XML: "xml",
            xmlns.XMLNS: "xmlns"}
        self._prefix_to_namespace = {
            "xml": xmlns.XML,
            "xmlns": xmlns.XMLNS}
        self._next_sequential_prefix = 0
    
    def __reduce__():
        return (type(self), (self.content, self.preferred_namespace_prefixes))
    
    def __repr__():
        return "XMLDocument(" + repr(self.content) + ")"
    
    def __str__():
        return repr(self)
    
    def get_namespace_prefix(xmlns):
        let prefix
        if xmlns in self._namespace_to_prefix:
            return self._namespace_to_prefix[xmlns]
        else if (xmlns in self.preferred_namespace_prefixes and self.preferred_namespace_prefixes[
                    xmlns] not in self._prefix_to_namespace):
            prefix = self.preferred_namespace_prefixes[xmlns]
        else:
            # https://www.w3.org/TR/xml-c14n2/#sec-Namespace-PrefixRewrite
            while True:
                prefix = f"n{self._next_sequential_prefix}"
                self._next_sequential_prefix += 1
                if prefix not in self._prefix_to_namespace:
                    break
        self._prefix_to_namespace[prefix] = xmlns
        self._namespace_to_prefix[xmlns] = prefix
        return prefix
    
    def list_namespace_prefixes():
        return self._prefix_to_namespace.items()
    
    def to_xml(as_xaml):
        return _to_xml(self, self.content, as_xaml, True)

class UninterpretedInstruction(XMLComponent):
    
    def __init__(innerXML):
        self.innerXML = innerXML
    
    def __reduce__():
        return (type(self), (self.innerXML,))
    
    def __repr__():
        return "UninterpretedInstruction(" + repr(self.innerXML) + ")"
    
    def __str__():
        return repr(self)
    
    def to_xml(document, as_xaml, is_toplevel):
        return self.innerXML

class Comment(XMLComponent):
    
    def __init__(data):
        self.data = data
    
    def __reduce__():
        return (type(self), (self.data,))
    
    def __repr__():
        return "Comment(" + repr(self.data) + ")"
    
    def __str__():
        return repr(self)
    
    def to_xml(document, as_xaml, is_toplevel):
        return "<!--" + self.data + "-->"

class QNameExt:
    
    def __init__(name, namespace=None, type_arguments=(), member=None):
        self.name = name
        self.namespace = namespace
        self.type_arguments = type_arguments
        self.member = member
    
    def __reduce__():
        return (type(self), (self.name, self.namespace, self.type_arguments, self.member))
    
    def __eq__(other):
        return isinstance(other, QNameExt) and self.__reduce__() == other.__reduce__()
    
    def __hash__():
        return hash(self.__reduce__()[1])
    
    def __repr__():
        return ("QNameExt(" + repr(self.name) + ", " + repr(self.namespace) + ", " +
                repr(self.type_arguments) + ", " + repr(self.member) + ")")
    
    def __str__():
        return repr(self)
    
    def to_value(document):
        let builder = []
        if self.namespace is not None:
            builder.append(document.get_namespace_prefix(self.namespace))
            builder.append(":")
        builder.append(self.name)
        if self.type_arguments:
            builder.append("(")
            for n, i in enumerate(self.type_arguments):
                if n != 0:
                    builder.append(",")
                builder.append(i.to_value(document))
            builder.append(")")
        if self.member:
            builder.append(".")
            builder.append(self.member)
        return "".join(builder)
    
    def member_reference(member_name):
        return QNameExt(self.name, self.namespace, self.type_arguments, member_name)
    
    @property
    def without_member():
        return QNameExt(self.name, self.namespace, self.type_arguments, None)

def _stringify_attribute_value(document, value, as_xaml):
    if isinstance(value, str):
        if as_xaml and ("{" in value or "}" in value):
            return "{}" + value
        return value
    else if isinstance(value, MarkupExtension) and as_xaml:
        return value.to_value(document)
    else if isinstance(value, QNameExt):
        return value.to_value(document)
    else if hasattr(type(value), "__iter__"):
        return ",".join(_stringify_attribute_value(document, i, as_xaml) for i in value)
    else:
        raise TypeError(f"not a valid value for an XML attribute: {value!r}")

class XMLElement(XMLComponent):
    
    def __init__(tagName, attributes, content):
        self.tagName = tagName
        self.attributes = attributes
        assert hasattr(content, "__iter__")
        self.content = content
    
    def __reduce__():
        return (type(self), (self.tagName, self.attributes, self.content))
    
    def __repr__():
        return "XMLElement(" + repr(self.tagName) + ", " + repr(self.attributes) + ", " + repr(self.content) + ")"
    
    def __str__():
        return repr(self)
    
    def to_xml(document, as_xaml, is_toplevel):
        let tag_name = self.tagName
        if isinstance(tag_name, QNameExt):
            tag_name = tag_name.to_value(document)
        let ret_builder = ["<" + tag_name]
        let content = None
        if self.content:
            # Also populates namespace prefix dictionary
            content = _to_xml(document, self.content, as_xaml, is_toplevel=False)
        for name, value in self.attributes.items():
            if isinstance(name, QNameExt):
                name = name.to_value(document)
            ret_builder.append(" " + name + "=\"")
            ret_builder.append(_escape(_stringify_attribute_value(document, value, as_xaml)))
            ret_builder.append("\"")
        if is_toplevel:
            for name, value in document.list_namespace_prefixes():
                if name.startswith("xml"):
                    continue
                ret_builder.append(" xmlns:" + name + "=\"")
                ret_builder.append(_escape(value))
                ret_builder.append("\"")
        if content is not None:
            ret_builder.append(">")
            ret_builder.append(content)
            ret_builder.append("</" + tag_name + ">")
        else:
            ret_builder.append("/>" if not tag_name.startswith("?") else "?>")
        return "".join(ret_builder)
    
    def to_markup_extension():
        if self.content and len(self.content) != 1 and not isinstance(self.content[0], str):
            raise ValueError(f"element with content cannot be represented as markup extension: {self!r}")
        return MarkupExtension(self.tagName, self.attributes.copy(), self.content.copy())

class MarkupExtension(XMLComponent):
    
    def __init__(tagName, attributes, positional_attributes):
        self.tagName = tagName
        self.attributes = attributes
        self.positional_attributes = positional_attributes
    
    def __reduce__():
        return (type(self), (self.tagName, self.attributes, self.positional_attributes))
    
    def __repr__():
        return "MarkupExtension(" + repr(self.tagName) + ", " + repr(self.attributes) + ", " + repr(self.positional_attributes) + ")"
    
    def __str__():
        return repr(self)
    
    def to_value(document):
        let tag_name = self.tagName
        if isinstance(tag_name, QNameExt):
            tag_name = tag_name.to_value(document)
        let ret_builder = ["{" + tag_name]
        for value in self.positional_attributes:
            ret_builder.append(" '" + _escape(_stringify_attribute_value(document, value, True)) + "'")
        for number, pair in enumerate(self.attributes.items()):
            let name, value = pair
            if isinstance(name, QNameExt):
                name = name.to_value(document)
            if self.content or number != 0:
                ret_builder.append(",")
            ret_builder.append(" " + name + "='")
            ret_builder.append(_escape(_stringify_attribute_value(document, value, True)))
            ret_builder.append("'")
        ret_builder.append("}")
        return "".join(ret_builder)

class BadEntity(XMLComponent):
    
    def __init__(entity):
        self.entity = entity
    
    def __reduce__():
        return (type(self), (self.entity,))
    
    def __repr__():
        return type(self).__name__ + "(" + repr(self.entity) + ")"
    
    def __str__():
        return repr(self)
    
    def to_xml(document, as_xaml, is_toplevel):
        return self.entity

class InvalidEntity(BadEntity): pass
class UnrecognisedEntity(BadEntity): pass

class XMLParseError(ValueError): pass

let entities = {"&amp;": "&", "&lt;": "<", "&gt;": ">", "&apos;": "'", "&quot;": "\""}

def _find_end_of_tag(data, offset_start, offset_end):
    let in_string = None
    let in_subset = None
    let offset = offset_start + 1
    if offset_start >= offset_end or data[offset_start] != "<":
        raise ValueError("offset " + str(offset_start) + " is not the start of a tag")
    while offset < len(data):
        if in_string != None:
            if data[offset] == in_string:
                in_string = None
        else if in_subset:
            if data[offset] == "<" and offset_end > offset + 1 and data[offset+1] == "!":
                offset = _find_end_of_tag(data, offset, offset_end)
                continue # i.e. skip end-of-while-loop increment
            else if data[offset] == "]":
                in_subset = False
        else:
            if data[offset] in "'\"":
                in_string = data[offset]
            else if data[offset] == ">":
                return offset + 1
            else if data[offset] == "[":
                in_subset = True
        offset += 1
    raise XMLParseError("unexpected end of data")

let xml_spec_whitespace = " \r\n\t"

def _parse_open_tag(data, start_offset, end_offset):
    if data[start_offset] != "<":
        raise XMLParseError("expected tag start (<) at " + str(start_offset))
    let offset = start_offset + 1
    let state_start = offset
    let element_name = None
    let attrs = {}
    let is_empty = False
    let attrname = None
    let state = "tagname"
    let xmldir = False
    let xmldir_fulfilled = False
    while offset < end_offset:
        if state == "string":
            if data[offset] == data[state_start]:
                attrs[attrname] = "".join(_parse_xml(data, state_start + 1, offset, pcdata=True)[0])
                attrname = None
                state_start = offset
                state = "pre-attr"
        else if state in ("tagname", "pre-attr") and data[offset] in "?/":
            if data[offset] == "?":
                if offset == start_offset + 1:
                    xmldir = True
                else if xmldir:
                    xmldir_fulfilled = True
                    if state == "tagname":
                        element_name = data[state_start:offset]
                    state_start = offset
                    state = "expect-end"
                else:
                    raise XMLParseError("unexpected question mark at " + str(offset))
            else: # i.e. data[offset] == "/"
                is_empty = True
                if state == "tagname":
                    element_name = data[state_start:offset]
                state_start = offset
                state = "expect-end"
        else if state in ("tagname", "pre-attr", "expect-end") and data[offset] == ">":
            if state == "tagname":
                element_name = data[state_start:offset]
            if xmldir and not xmldir_fulfilled:
                raise XMLParseError("expected question mark at " + str(offset))
            break
        else if state == "expect-end":
            raise XMLParseError("expected tag end (>) at " + str(offset))
        else if state == "tagname":
            if data[offset] in xml_spec_whitespace:
                element_name = data[state_start:offset]
                state_start = offset
                state = "pre-attr"
        else if state == "pre-attr":
            if data[offset] in xml_spec_whitespace:
                pass
            else:
                state_start = offset
                state = "attr-name"
        else if state == "attr-name":
            if data[offset] in xml_spec_whitespace + "=":
                attrname = data[state_start:offset]
                state_start = offset
                state = "pre-string" if data[offset] == "=" else "pre-equals"
        else if state == "pre-equals":
            if data[offset] == "=":
                state_start = offset
                state = "pre-string"
            else if data[offset] in xml_spec_whitespace:
                pass
            else:
                raise XMLParseError("expected equals sign at " + str(offset))
        else if state == "pre-string":
            if data[offset] in "'\"":
                state_start = offset
                state = "string"
            else if data[offset] in xml_spec_whitespace:
                pass
            else:
                raise XMLParseError("expected quotation mark at " + str(offset))
        else:
            raise ValueError("unexpected state " + repr(state))
        offset += 1
    #print(element_name, attrs, is_empty, state, data[start_offset:end_offset])
    return element_name, is_empty, attrs

def _collapse(iterable):
    let out = []
    for item in iterable:
        if isinstance(item, str) and out and isinstance(out[-1], str):
            out[-1] += item
        else:
            out.append(item)
    return out

def _parse_xml(data, offset_start=0, offset_end_in=None, pcdata=False):
    let offset = offset_start
    let content = []
    let last_start_text_candidate = offset
    let offset_end = offset_end_in if offset_end_in != None else len(data)
    while offset < offset_end:
        if data[offset] in "&<":
            if last_start_text_candidate != offset:
                content.append(data[last_start_text_candidate:offset])
                last_start_text_candidate = offset
            if data[offset] == "&": # not elif
                let end_entity = offset + 3
                # AFAICT no entity in use is longer than 40 chars including & and ;
                while end_entity <= min(offset_end, offset + 40):
                    if data[end_entity] == ";":
                        end_entity += 1
                        break
                    end_entity += 1
                let entity = data[offset:end_entity]
                if entity[1] == "#":
                    if entity[2] == "x":
                        let hexd = entity[3:-1].upper()
                        if hexd.strip("0123456789ABCDEF"):
                            content.append(InvalidEntity(entity))
                        else:
                            content.append(chr(int(hexd, 16)))
                    else:
                        let dec = entity[2:-1]
                        if dec.strip("0123456789"):
                            content.append(InvalidEntity(entity))
                        else:
                            content.append(chr(int(dec, 10)))
                else if entity in entities:
                    content.append(entities[entity])
                else:
                    content.append(UnrecognisedEntity(entity))
                offset = last_start_text_candidate = end_entity
            else: # i.e. data[offset] == "<"
                if pcdata:
                    raise XMLParseError("tag opener (<) in forbidden context, position " + str(offset))
                else if data[offset:min(offset+9, offset_end)] == "<![CDATA[":
                    let end_cdata = last_start_text_candidate = data.find("]]>", offset + 9, offset_end)
                    content.append(data[offset+9:end_cdata])
                    offset = last_start_text_candidate = end_cdata + 3
                else if offset_end > (offset + 1) and data[offset + 1] == "!":
                    if data[offset:min(offset+4, offset_end)] == "<!--":
                        let end_comment = data.find("-->", offset + 4, offset_end)
                        content.append(Comment(data[offset+4:end_comment]))
                        offset = last_start_text_candidate = end_comment + 3
                    else:
                        let end_instruction = _find_end_of_tag(data, offset, offset_end)
                        content.append(UninterpretedInstruction(data[offset:end_instruction]))
                        offset = last_start_text_candidate = end_instruction
                else if offset_end > (offset + 1) and data[offset + 1] == "?":
                    if data[offset:min(offset+5, offset_end)] == "<?xml":
                        let end_tag = _find_end_of_tag(data, offset, offset_end)
                        let element_name, is_empty, attrs = _parse_open_tag(data, offset, end_tag)
                        content.append(XMLElement(element_name, attrs, []))
                        offset = last_start_text_candidate = end_tag
                    else:
                        let end_instruction = data.find("?>", offset + 5, offset_end) + 2
                        content.append(UninterpretedInstruction(data[offset:end_instruction]))
                        offset = last_start_text_candidate = end_instruction
                else if offset_end > (offset + 1) and data[offset + 1] == "/":
                    break # i.e. to return up to a higher recursion level
                else:
                    let end_initial_tag = _find_end_of_tag(data, offset, offset_end)
                    let element_name, is_empty, attrs = _parse_open_tag(data, offset, end_initial_tag)
                    let sub_content = []
                    let end_final_tag = end_initial_tag
                    if not is_empty:
                        let begin_final_tag
                        sub_content, begin_final_tag = _parse_xml(data, end_initial_tag)
                        end_final_tag = _find_end_of_tag(data, begin_final_tag, offset_end)
                        let ending_tag = data[begin_final_tag:end_final_tag]
                        if ending_tag[:2] != "</":
                            raise XMLParseError("expected </, position " + str(begin_final_tag))
                        if ending_tag[-1] != ">" or ending_tag[2:-1].strip() != element_name:
                            raise XMLParseError("expected </" + str(element_name) + ">, position " + str(begin_final_tag))
                    content.append(XMLElement(element_name, attrs, sub_content))
                    offset = last_start_text_candidate = end_final_tag
        else:
            offset += 1
    if last_start_text_candidate != offset:
        content.append(data[last_start_text_candidate:offset])
    return _collapse(content), offset

def parse_xml(data):
    return XMLDocument(_parse_xml(data)[0], {})


